package eiffel.da6.basic_rest.utils;

import eiffel.da6.basic_rest.Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {

    private static Connection con;

    private static String getDbUrl() {
        StringBuilder sb = new StringBuilder();
        sb.append("jdbc:mysql://").append(Main.dotenv.get("DB_HOST")).append(":").append(Main.dotenv.get("DB_PORT"))
                .append("/").append(Main.dotenv.get("DB_NAME")).append("?")
                .append("useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowMultiQueries=true");
        return sb.toString();
    }
    public static Connection getDBConnection() {
        try {
            if (con ==null) {
                Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
                con = DriverManager.getConnection(getDbUrl(), "basic", "rest");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return con;
    }
}
